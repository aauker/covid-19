import torch
from torch import nn
from torch.utils import data
from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision import models
from PIL import Image
import matplotlib.pyplot as plt

import numpy as np
import pandas as pd
from datetime import datetime
import sys
import glob
import cv2
import os
import random
import xlrd
import time
import json

from PIL import Image
from PIL import ImageStat

from sklearn.utils import class_weight
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report

class CovidXRayStreamDataset(data.Dataset):
    """Dataset reader for covid-19 ieee dataset."""

    def __init__(self, database='./covid-chestxray-dataset/metadata.csv'):
        print ("Hello @.@")
        self.manifest = pd.read_csv(database, index_col=0).dropna(subset=['survival'])
        self.manifest.drop( self.manifest[self.manifest['modality'] != 'X-ray'].index, inplace=True)
        self.manifest['label'] = np.where(self.manifest['survival'] == 'Y', 0, 1)
        print (self.manifest)

        self.train_manifest, self.test_manifest = train_test_split(self.manifest, test_size=0.10)
        print (self.train_manifest)
        print (self.test_manifest)

        self.img_finalize = transforms.Compose([
            transforms.Resize(128),
            transforms.RandomHorizontalFlip(p=0.5),
            transforms.RandomVerticalFlip(p=0.5),
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
        ])

    def GetClassWeights(self):
        return torch.tensor(class_weight.compute_class_weight('balanced', np.unique(self.manifest['label']), self.manifest['label'])).float()

    def train(self):      self.mode = 'train'
    def build(self):      self.mode = 'build'
    def eval(self):       self.mode = 'eval'
    def interface(self):  self.mode = 'interface'

    def __len__(self):
        if   self.mode == 'train':      return len(self.train_manifest)
        elif self.mode == 'eval':       return len(self.test_manifest)
        elif self.mode == 'interface':  return len(self.manifest)
        else: print ("Yikes!")

    def __getitem__(self, idx):
        torch.set_default_tensor_type('torch.FloatTensor')
        if self.mode == 'train':
            tile = Image.open('./covid-chestxray-dataset/images/' + self.train_manifest.iloc[idx]['filename']).convert('RGB')
            print (tile)
            label = self.train_manifest.iloc[idx]['label']
            return self.img_finalize(tile), label
        elif self.mode == 'eval':
            tile = Image.open('./covid-chestxray-dataset/images/' + self.test_manifest.iloc[idx]['filename']).convert('RGB')
            label = self.test_manifest.iloc[idx]['label']
            return self.img_finalize(tile), label
        elif self.mode == 'interface':
            raise NotImplementedError("No")
        else:
            print ("Yikes!")

if __name__ == "__main__":
    output_dir = '.'
    # resnet = models.resnet18(pretrained=False, num_classes=8)
    # optimizer = torch.optim.Adam(resnet.parameters(), betas=(0.9,0.999), lr=0.001)
    dataset = CovidXRayStreamDataset()
    print (dataset.GetClassWeights())

    print ("Testing")
    dataset.train()
    dataloader = DataLoader(dataset, batch_size=1, num_workers=4, shuffle=True)
    for batch_idx, master_batch_data in enumerate(dataloader):
        image_data  = master_batch_data[0].float()
        labels_data = master_batch_data[1].long()
        plt.imshow(image_data.squeeze(0).permute(1,2,0),vmin=0, vmax=1)
        plt.show()
        print (image_data.shape)

